const readline = require('readline');
const sharp = require('sharp');

const readlineInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function ask(questionText) {
  return new Promise((resolve, _) => {
    readlineInterface.question(questionText, (input) => resolve(input));
  });
}

function makeImage(image, multiplier = 1, format) {
  const file = image.file;
  const fileName = file.split('/').pop().split('.').slice(0, -1).join('.');
  const path = file.split('/').slice(0, -1).join('/');
  const extension = file.split('.').pop();
  const scaling = `@${multiplier}x`;
  return new Promise((resolve, _) => {
    if (format === 'webp') {
      sharp(file)
        .resize({ width: image.width * multiplier })
        .webp()
        .toFile(`${path}/${fileName}${scaling}.webp`)
        .then(() => resolve(`${path}/${fileName}${scaling}.webp`));
    } else {
      sharp(file)
        .resize({ width: image.width * multiplier })
        .toFile(`${path}/${fileName}${scaling}.${extension}`)
        .then(() => resolve(`${path}/${fileName}${scaling}.${extension}`));
    }
  });
}

transformImages().then((imageList) => {
  console.log(imageList);
  process.exit();
});

async function transformImages() {
  const image = await getInput();
  let one = await makeImage(image);
  let two = await makeImage(image, 2);
  let three = await makeImage(image, 3);
  let oneWebP = await makeImage(image, 1, 'webp');
  let twoWebP = await makeImage(image, 2, 'webp');
  let threeWebP = await makeImage(image, 3, 'webp');
  return {
    '1x': `${one}, ${oneWebP}`,
    '2x': `${two}, ${twoWebP}`,
    '3x': `${three}, ${threeWebP}`,
  };
}

async function getInput() {
  let file = await ask('Image file path: ');
  let width = await ask('Desired width at 1x: ');
  return { file, width: parseInt(width, 10) };
}
