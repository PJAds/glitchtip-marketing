import { Component } from '@angular/core';
import { LinksService } from './links.service';
import { SocialTagsServiceService } from './social-tags-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'glitchtip-marketing';
  registerLink = this.links.registerLink;

  public constructor(
    private tagsService: SocialTagsServiceService,
    private links: LinksService
  ) {
    this.tagsService.setTitleAndTags();
  }
}
