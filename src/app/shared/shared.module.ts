import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { RouterModule } from '@angular/router';
import { ResponsiveImageComponent } from './responsive-image/responsive-image.component';
import { PaymentComponent } from './payment/payment.component';

@NgModule({
  declarations: [ResponsiveImageComponent, PaymentComponent],
  exports: [ResponsiveImageComponent, PaymentComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    RouterModule,
  ],
})
export class SharedModule {}
