import { Component, ViewEncapsulation } from '@angular/core';
import { ScullyRoutesService } from '@scullyio/ng-lib';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss'],
  preserveWhitespaces: true,
  encapsulation: ViewEncapsulation.Emulated,
})
export class PricingComponent {
  constructor(private scully: ScullyRoutesService) {}

  pricingRoutes$ = this.scully.available$.pipe(
    map((availableRoutes) =>
      availableRoutes
        .filter((routeObject) => routeObject.route.startsWith('/pricing'))
        .reverse()
    )
  );
}
