import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ScullyRoutesService } from '@scullyio/ng-lib';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SocialTagsServiceService {
  constructor(
    private titleService: Title,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private scully: ScullyRoutesService,
    private meta: Meta
  ) {}

  get data() {
    return this.activatedRoute.snapshot.firstChild!.data;
  }

  setTitleAndTags() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map((route) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter((route) => route.outlet === 'primary')
      )
      .subscribe(() => {
        this.scully.getCurrent().subscribe((link) => {
          if (link?.title) {
            this.titleService.setTitle('GlitchTip | ' + link.title);
            this.meta.updateTag({
              name: 'og:title',
              property: 'og:title',
              content: link.title,
            });
            this.meta.updateTag({
              name: 'og:description',
              property: 'og:description',
              content: link.description,
            });
            this.meta.updateTag({
              name: 'description',
              content: link.description,
            });
          }
        });
      });
  }
}
