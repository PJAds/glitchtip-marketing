import { Component, ViewEncapsulation } from '@angular/core';
import { ScullyRoutesService } from '@scullyio/ng-lib';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
  preserveWhitespaces: true,
})
export class BlogComponent {
  current$ = this.scully.getCurrent();
  title$ = this.current$.pipe(map((route) => route.title));
  author$ = this.current$.pipe(map((route) => route.author));
  date$ = this.current$.pipe(map((route) => route.date));

  constructor(private scully: ScullyRoutesService) {}
}
