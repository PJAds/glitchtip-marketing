# GlitchtipMarketing

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Making new blog posts
1. Create new markdown file, add images to blog images folder (see below for optimization)
2. Run `ng build` followed by `npm run scully` to update site routes.
3. Static pages like blogs will not be accesible by running ng serve. To view updated site run `npm run scully:serve`. 

## Optimizing images

Dropping in a screenshot that's way too big?

The `transform-image` script accepts an image path and a desired width at 1x. It then generates 1x, 2x, and 3x images in both the image's current form and in WebP.

`npm run transform-image` It does not support arguments at this time.

The width is in [CSS pixels](https://developer.mozilla.org/en-US/docs/Glossary/CSS_pixel).

You can then put these images into the site, either with the `<app-responsive-image>` component, or if you're in a markdown file you have to use the <picture> tag wrapped in a div. There are examples of both of these in the repo to inspire you.

The script and the Angular component assume that you're working with a big image that would benefit from 1x, 2x, and 3x scaling. If your image is smaller, you might not need this at all.
